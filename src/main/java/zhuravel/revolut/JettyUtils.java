package zhuravel.revolut;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class JettyUtils {
    public static void runServer(Object service) throws Exception {
        int port = Integer.parseInt(System.getProperty("server.port", "8123"));

        URI baseUri = UriBuilder.fromUri("http://localhost/").port(port).build();
        ResourceConfig resourceConfig = new ResourceConfig()
                .register(service)
                .register(MoxyJsonFeature.class);

        Server server = JettyHttpContainerFactory.createServer(baseUri, resourceConfig);
        server.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                server.stop();
            } catch (Exception e) {
                // ignore
            }
        }));
    }
}
