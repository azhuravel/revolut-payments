package zhuravel.revolut.exception;

public class TransferSameAccountException extends RuntimeException {
    public TransferSameAccountException(String message) {
        super(message);
    }
}
