package zhuravel.revolut.service;

import java.util.function.BiConsumer;

import zhuravel.revolut.entity.Account;

public interface AccountService {
    Account findById(long id);

    void saveOrUpdate(Account account);

    void executeInLock(Long id1, Long id2, BiConsumer<Account, Account> operation);
}
