package zhuravel.revolut.service.impl;

import java.math.BigDecimal;

import zhuravel.revolut.entity.Account;
import zhuravel.revolut.exception.InsufficientBalanceException;
import zhuravel.revolut.exception.InvalidAccountException;
import zhuravel.revolut.exception.InvalidAmountException;
import zhuravel.revolut.exception.TransferSameAccountException;
import zhuravel.revolut.service.AccountService;
import zhuravel.revolut.service.PaymentService;

public class PaymentServiceImpl implements PaymentService {

    private AccountService accountService;

    public PaymentServiceImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    public void transfer(long accountIdFrom, long accountIdTo, BigDecimal amount) throws InsufficientBalanceException, TransferSameAccountException {
        validate(accountIdFrom, accountIdTo, amount);
        accountService.executeInLock(accountIdFrom, accountIdTo, (Account from, Account to) -> {
            ensureBalanceEnough(from, amount);

            BigDecimal fromBalance = from.getBalance().subtract(amount);
            BigDecimal toBalance = to.getBalance().add(amount);

            from.setBalance(fromBalance);
            to.setBalance(toBalance);

            accountService.saveOrUpdate(from);
            accountService.saveOrUpdate(to);
        });
    }

    private void validate(long from, long to, BigDecimal amount) {
        if (from <= 0 || to <= 0) {
            throw new InvalidAccountException("Account ID should be greater than 0");
        }

        if (amount == null || BigDecimal.ZERO.compareTo(amount) > 0) {
            throw new InvalidAmountException("Amount should be greater than 0");
        }

        if (to == from) {
            throw new TransferSameAccountException("Transfer is only possible between different accounts");
        }
    }

    private void ensureBalanceEnough(Account from, BigDecimal amount) throws InsufficientBalanceException {
        if (amount.compareTo(from.getBalance()) > 0)
            throw new InsufficientBalanceException("Balance is not enough to make the transfer");
    }

}
