package zhuravel.revolut.api.impl;

import javax.ws.rs.core.Response;

import zhuravel.revolut.entity.Account;
import zhuravel.revolut.exception.AccountNotFoundException;
import zhuravel.revolut.exception.InsufficientBalanceException;
import zhuravel.revolut.exception.InvalidAccountException;
import zhuravel.revolut.exception.InvalidAmountException;
import zhuravel.revolut.exception.InvalidBalanceException;
import zhuravel.revolut.exception.TransferSameAccountException;
import zhuravel.revolut.api.AccountRemoteService;
import zhuravel.revolut.api.dto.AccountDTO;
import zhuravel.revolut.api.dto.TransferDTO;
import zhuravel.revolut.service.AccountService;
import zhuravel.revolut.service.PaymentService;

public class AccountRemoteServiceImpl implements AccountRemoteService {
    private final AccountService accountService;
    private final PaymentService paymentService;

    public AccountRemoteServiceImpl(AccountService accountService, PaymentService paymentService) {
        this.accountService = accountService;
        this.paymentService = paymentService;
    }

    @Override
    public Response create(AccountDTO accountDTO) {
        if (accountDTO == null || accountDTO.getId() != null)
            return Response.status(Response.Status.BAD_REQUEST).entity("Only field 'balance' should be set").build();

        try {
            Account account = accountDTO.toAccount();
            accountService.saveOrUpdate(account);
            return Response.status(Response.Status.CREATED).entity(AccountDTO.fromAccount(account)).build();
        } catch (InvalidBalanceException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }

    @Override
    public Response get(long id) {
        try {
            Account account = accountService.findById(id);
            AccountDTO accountDTO = AccountDTO.fromAccount(account);
            return Response.ok().entity(accountDTO).build();
        } catch (AccountNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
        }
    }

    @Override
    public Response transfer(TransferDTO transferDTO) {
        try {
            paymentService.transfer(transferDTO.getFrom(), transferDTO.getTo(), transferDTO.getAmount());
            return Response.ok().build();
        } catch (InvalidAccountException | InvalidAmountException | TransferSameAccountException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        } catch (InsufficientBalanceException | AccountNotFoundException e) {
            // 422 Unprocessable Entity
            return Response.status(422).entity(e.getMessage()).build();
        }
    }
}
