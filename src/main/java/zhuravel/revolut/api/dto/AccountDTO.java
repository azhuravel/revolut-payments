package zhuravel.revolut.api.dto;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;
import zhuravel.revolut.entity.Account;

@Data
@Accessors(chain = true)
public class AccountDTO {
    private Long id;

    private BigDecimal balance;

    public Account toAccount() {
        Account account = new Account();
        account.setId(id);
        account.setBalance(balance);

        return account;
    }

    public static AccountDTO fromAccount(Account account) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(account.getId());
        accountDTO.setBalance(account.getBalance());

        return accountDTO;
    }
}
