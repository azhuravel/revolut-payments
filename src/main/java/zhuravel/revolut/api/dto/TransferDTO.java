package zhuravel.revolut.api.dto;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TransferDTO {
    private long from;

    private long to;

    private BigDecimal amount;
}
