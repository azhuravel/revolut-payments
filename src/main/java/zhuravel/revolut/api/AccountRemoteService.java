package zhuravel.revolut.api;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import zhuravel.revolut.api.dto.AccountDTO;
import zhuravel.revolut.api.dto.TransferDTO;

@Path("/account")
public interface AccountRemoteService {
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    Response create(AccountDTO accountDTO);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    Response get(@PathParam("id") long id);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/transfer")
    Response transfer(TransferDTO transferDTO);
}
