package zhuravel.revolut.entity;

import java.math.BigDecimal;
import java.util.concurrent.locks.ReentrantLock;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Account {
    private Long id;
    private BigDecimal balance;
    private ReentrantLock lock = new ReentrantLock();
}
